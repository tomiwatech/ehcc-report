## ehcc-report

ehcc-report is an api for managing reports at Royal house of grace

---

## Setup

    $ git clone https://gitlab.com/tomiwatech/ehcc-report.git/
    $ cd ehcc-report

## Configure app

check the `config.yml` file, modify it and add the correct values.

## Running the project

To run the project locally, execute the command below

`make build_and_serve`

## Integration Tests

To run integration tests run

`make integration_tests`

## Acceptance Tests

To run acceptance/end to end tests run

`make acceptance_tests`

This will start the database and application

## Documentation

[Postman Collection](https://documenter.getpostman.com/view/3064040/VUjSGisx)
