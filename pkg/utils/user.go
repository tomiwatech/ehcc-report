package utils

import (
	"context"
	"fmt"
	"github.com/go-playground/validator/v10"
	"gitlab.com/tomiwatech/ehcc-report/internal/database"
	. "gitlab.com/tomiwatech/ehcc-report/internal/user"
)

func ValidateRequest(requestBody interface{}) []ErrorResponse {
	var validate = validator.New()
	var errors []ErrorResponse
	err := validate.Struct(requestBody)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ErrorResponse
			element.FailedField = err.Field()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, element)
		}
	}

	return errors
}

func GetUserByEmail(email string) (UserDTO, error) {
	db := database.Connection()
	userService := NewUser(&db)
	user, err := userService.GetUserByEmail(context.Background(), email)

	if err != nil {
		fmt.Println(err)
		return UserDTO{}, ErrorFetchingUser
	}

	return user, nil
}
