package utils

import (
	"encoding/json"
	"net/http"
)

func ApiResponseWithError(writer http.ResponseWriter, code int, msg string) {
	ApiResponse(writer, code, map[string]string{"error": msg})
}

func ApiResponse(writer http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(code)
	_, err := writer.Write(response)
	if err != nil {
		return
	}
}
