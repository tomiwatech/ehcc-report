package user

import (
	"context"
	"errors"
	"fmt"
)

var (
	ErrorFetchingUser                  = errors.New("failed to fetch user by id")
	ErrorUserAlreadyExist              = errors.New("user with email already exists")
	ErrorWrongEmailPasswordCombination = errors.New("wrong email and password combination")
	ErrorWrongPasswordEmailCombination = errors.New("wrong password and email combination")
	ErrorCreatingUser                  = errors.New("failed to create a new user")
)

type LoginRequest struct {
	Email    string `validate:"required" json:"email"`
	Password string `validate:"required" json:"password"`
}
type ErrorResponse struct {
	FailedField string
	Tag         string
	Value       string
}

type TokenResponse struct {
	Email string `json:"email"`
	Token string `json:"token"`
}

type User struct {
	FirstName     string `validate:"required" json:"firstName"`
	LastName      string `validate:"required" json:"lastName"`
	Email         string `validate:"required,email,min=6,max=32" json:"email"`
	Department    string `validate:"required" json:"department"`
	Designation   string `validate:"required" json:"designation"`
	ContactNumber string `validate:"required" json:"contactNumber"`
	Password      string `validate:"required,min=6,max=20" json:"password"`
}

type UserDTO struct {
	ID            string `bson:"_id"`
	FirstName     string `bson:"firstName"`
	LastName      string `bson:"lastName"`
	Email         string `bson:"email"`
	Department    string `bson:"department"`
	Designation   string `bson:"designation"`
	ContactNumber string `bson:"contactNumber"`
	Password      string `bson:"password"`
}

type UserStore interface {
	GetUser(context.Context, string) (UserDTO, error)
	GetUserByEmail(context.Context, string) (UserDTO, error)
	GetUsers(context.Context) ([]UserDTO, error)
	CreateUser(context.Context, User) (User, error)
}

type Service struct {
	UserStore UserStore
}

func NewUser(UserStore UserStore) *Service {
	return &Service{
		UserStore: UserStore,
	}
}

func ToUserDTO(user User) UserDTO {
	return UserDTO{
		FirstName:     user.FirstName,
		LastName:      user.LastName,
		Email:         user.Email,
		Department:    user.Department,
		Designation:   user.Designation,
		ContactNumber: user.ContactNumber,
		Password:      user.Password,
	}
}

func ToUserModel(userDTO UserDTO) User {
	return User{
		FirstName:     userDTO.FirstName,
		LastName:      userDTO.LastName,
		Email:         userDTO.Email,
		Department:    userDTO.Department,
		Designation:   userDTO.Designation,
		ContactNumber: userDTO.ContactNumber,
	}
}

func (s *Service) GetUser(ctx context.Context, id string) (UserDTO, error) {
	fmt.Println("Retrieving a user")
	user, err := s.UserStore.GetUser(ctx, id)
	if err != nil {
		fmt.Println(err)
		return UserDTO{}, ErrorFetchingUser
	}

	return user, nil
}

func (s *Service) GetUserByEmail(ctx context.Context, email string) (UserDTO, error) {
	fmt.Println("Retrieving a user")
	user, err := s.UserStore.GetUserByEmail(ctx, email)
	if err != nil {
		fmt.Println(err)
		return UserDTO{}, ErrorFetchingUser
	}

	return user, nil
}

func (s *Service) GetUsers(ctx context.Context) ([]UserDTO, error) {
	fmt.Println("Retrieving users")
	users, err := s.UserStore.GetUsers(ctx)
	if err != nil {
		fmt.Println(err)
		return []UserDTO{}, ErrorFetchingUser
	}

	return users, nil
}

func (s *Service) CreateUser(ctx context.Context, user User) (User, error) {
	fmt.Println("creating a user")
	user, err := s.UserStore.CreateUser(ctx, user)
	if err != nil {
		fmt.Println(err)
		return User{}, ErrorCreatingUser
	}
	return user, nil
}
