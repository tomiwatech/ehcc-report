package database

import (
	"context"
	"fmt"
	"log"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"gitlab.com/tomiwatech/ehcc-report/internal/user"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (d *Database) GetUser(
	ctx context.Context,
	id string,
) (user.UserDTO, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.UserCollection)
	var dbUser user.UserDTO
	filter := bson.D{{Key: "_id", Value: id}}
	err := collection.FindOne(ctx, filter).Decode(&dbUser)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Found a single user: %+v\n", dbUser)

	return dbUser, nil
}

func (d *Database) GetUserByEmail(
	ctx context.Context,
	email string,
) (user.UserDTO, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.UserCollection)
	var dbUser user.UserDTO
	filter := bson.D{{Key: "email", Value: email}}
	err := collection.FindOne(ctx, filter).Decode(&dbUser)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return user.UserDTO{}, nil
		}
		log.Fatal(err)
	}

	fmt.Printf("Found a single user: %+v\n", dbUser)

	return dbUser, nil
}

func (d *Database) GetUsers(ctx context.Context) ([]user.UserDTO, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.UserCollection)
	filter := bson.D{}
	cursor, err := collection.Find(ctx, filter)
	if err != nil {
		panic(err)
	}

	var users []user.UserDTO
	if err = cursor.All(ctx, &users); err != nil {
		panic(err)
	}

	return users, nil
}

func (d *Database) CreateUser(ctx context.Context, userInfo user.User) (user.User, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.UserCollection)
	toUserDTO := user.ToUserDTO(userInfo)
	toUserDTO.ID = uuid.New().String()
	insertResult, err := collection.InsertOne(ctx, toUserDTO)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Inserted a single user: ", insertResult.InsertedID)

	var dbUser user.UserDTO
	filter := bson.D{{Key: "_id", Value: insertResult.InsertedID}}
	err = collection.FindOne(ctx, filter).Decode(&dbUser)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Found a single user: %+v\n", dbUser)
	userModel := user.ToUserModel(dbUser)

	return userModel, nil
}
