package database

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/tomiwatech/ehcc-report/internal/config"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var configInfo config.Config

func init() {
	configInfo.Read()
}

type Database struct {
	Client *mongo.Client
}

func NewDatabase() (*Database, error) {
	clientOptions := options.Client().ApplyURI(configInfo.Database.Uri)
	dbClient, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Panic(err)
	}

	return &Database{Client: dbClient}, nil
}

func (d *Database) Ping(ctx context.Context) error {
	fmt.Println("Connected to MongoDB!")
	return d.Client.Ping(context.TODO(), nil)
}

func Connection() Database {
	db, dbErr := NewDatabase()
	if dbErr != nil {
		fmt.Println("failed to connect to the database")
		log.Panic(dbErr)
	}

	return *db
}
