package routes

import (
	"github.com/gorilla/mux"
	. "gitlab.com/tomiwatech/ehcc-report/cmd/controller"
)

func UserRouter(router *mux.Router) *mux.Router {
	subRouter := router.PathPrefix("/users").Subrouter()
	subRouter.HandleFunc("", GetUsers).Methods("GET")
	subRouter.HandleFunc("/login", LoginUser).Methods("POST")
	subRouter.HandleFunc("/signup", CreateUser).Methods("POST")
	subRouter.HandleFunc("/{id}", GetUser).Methods("GET")
	return subRouter
}
