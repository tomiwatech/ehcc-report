package routes

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tomiwatech/ehcc-report/cmd/controller"
	"net/http"
)

func Routers() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	IndexRouter(router)
	subRouter := router.PathPrefix("/api").Subrouter()
	UserRouter(subRouter)
	router.Use(loggingMiddleware)
	return router
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info(r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func IndexRouter(router *mux.Router) *mux.Router {
	router.HandleFunc("/", controller.HealthCheck).Methods("GET")
	router.HandleFunc("/api", controller.ApiVersionIndex).Methods("GET")
	return router
}
