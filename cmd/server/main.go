package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/tomiwatech/ehcc-report/cmd/routes"
	"gitlab.com/tomiwatech/ehcc-report/internal/config"
)

var configInfo config.Config

func init() {
	configInfo.Read()
}

func main() {
	log.SetOutput(os.Stdout)
	logFormatter := new(config.LogFormatter)
	logFormatter.TimestampFormat = "2006-01-02 15:04:05"
	logFormatter.LevelDesc = []string{"PANIC", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE"}
	log.SetFormatter(logFormatter)

	router := routes.Routers()

	addr := fmt.Sprintf("0.0.0.0:%s", configInfo.Server.Port)
	srv := &http.Server{
		Addr: addr,
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      router,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	fmt.Println("Application running on port ", configInfo.Server.Port)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	// Create a deadline to wait for
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	err := srv.Shutdown(ctx)
	if err != nil {
		return
	}

	log.Println("shutting down gracefully")
}
