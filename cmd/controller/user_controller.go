package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	. "gitlab.com/tomiwatech/ehcc-report/pkg/utils"
	"golang.org/x/crypto/bcrypt"

	"github.com/gorilla/mux"
	"gitlab.com/tomiwatech/ehcc-report/internal/database"
	. "gitlab.com/tomiwatech/ehcc-report/internal/user"
)

var CreateUser = func(w http.ResponseWriter, request *http.Request) {
	var userInfo User
	decoder := json.NewDecoder(request.Body)
	decodeErr := decoder.Decode(&userInfo)
	if decodeErr != nil {
		ApiResponseWithError(w, http.StatusBadRequest, "Invalid JSON")
		return
	}

	errors := ValidateRequest(userInfo)

	if errors != nil {
		errMessage := ErrorMessage{
			Code:    http.StatusBadRequest,
			Message: "Error processing request",
			Status:  "error",
			Errors:  errors,
		}
		ApiResponse(w, http.StatusBadRequest, errMessage)
		return
	}

	userDetail, userErr := GetUserByEmail(userInfo.Email)

	if userErr != nil {
		ApiResponseWithError(w, http.StatusInternalServerError, userErr.Error())
		return
	}

	if userDetail.Email != "" && userDetail.Email == userInfo.Email {
		errMessage := ErrorMessage{
			Code:    http.StatusBadRequest,
			Message: ErrorUserAlreadyExist.Error(),
			Status:  "error",
			Errors:  nil,
		}
		ApiResponse(w, http.StatusBadRequest, errMessage)
		return
	}

	// Hashing the password with the default cost of 10
	password := []byte(userInfo.Password)
	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	db := database.Connection()
	userService := NewUser(&db)
	createForm, err := userService.CreateUser(context.Background(), User{
		FirstName:     userInfo.FirstName,
		LastName:      userInfo.LastName,
		Email:         userInfo.Email,
		Department:    userInfo.Department,
		Designation:   userInfo.Designation,
		ContactNumber: userInfo.ContactNumber,
		Password:      string(hashedPassword),
	})

	if err != nil {
		fmt.Println(err)
		ApiResponseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	response := Message{
		Code:    http.StatusCreated,
		Message: "User created successfully",
		Status:  "success",
		Data:    createForm,
	}

	ApiResponse(w, http.StatusCreated, response)
}

var GetUser = func(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	fmt.Println(id)

	if len(id) == 0 {
		ApiResponseWithError(w, http.StatusBadRequest, "Invalid request payload, please supply user id")
		return
	}

	db := database.Connection()
	userService := NewUser(&db)
	userResponse, err := userService.GetUser(context.Background(), id)

	if err != nil {
		fmt.Println(err)
		ApiResponseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	response := Message{
		Code:    http.StatusOK,
		Message: "Successfully fetched user",
		Status:  "success",
		Data:    userResponse,
	}

	ApiResponse(w, http.StatusOK, response)
}

var GetUsers = func(w http.ResponseWriter, r *http.Request) {

	db := database.Connection()
	userService := NewUser(&db)
	users, err := userService.GetUsers(context.Background())
	if err != nil {
		fmt.Println(err)
	}

	response := Message{
		Code:    http.StatusOK,
		Message: "Successfully fetched users",
		Status:  "success",
		Data:    users,
	}
	ApiResponse(w, http.StatusOK, response)
}

var LoginUser = func(writer http.ResponseWriter, request *http.Request) {
	var loginRequest LoginRequest
	decoder := json.NewDecoder(request.Body)
	decodeErr := decoder.Decode(&loginRequest)
	if decodeErr != nil {
		ApiResponseWithError(writer, http.StatusBadRequest, "Invalid JSON")
		return
	}

	errors := ValidateRequest(loginRequest)

	if errors != nil {
		errMessage := ErrorMessage{
			Code:    http.StatusBadRequest,
			Message: "Error processing request",
			Status:  "error",
			Errors:  errors,
		}
		ApiResponse(writer, http.StatusBadRequest, errMessage)
		return
	}

	dbUser, userErr := GetUserByEmail(loginRequest.Email)

	if userErr != nil {
		ApiResponseWithError(writer, http.StatusInternalServerError, userErr.Error())
		return
	}

	if dbUser.Email == "" {
		errMessage := ErrorMessage{
			Code:    http.StatusBadRequest,
			Message: ErrorWrongEmailPasswordCombination.Error(),
			Status:  "error",
			Errors:  nil,
		}
		ApiResponse(writer, http.StatusBadRequest, errMessage)
		return
	}

	// Comparing the password with the hash
	dbPassword := []byte(dbUser.Password)
	loginPassword := []byte(loginRequest.Password)
	passwordErr := bcrypt.CompareHashAndPassword(dbPassword, loginPassword)

	if passwordErr != nil {
		errMessage := ErrorMessage{
			Code:    http.StatusBadRequest,
			Message: ErrorWrongPasswordEmailCombination.Error(),
			Status:  "error",
			Errors:  nil,
		}
		ApiResponse(writer, http.StatusBadRequest, errMessage)
		return
	}

	token, tokenErr := GenerateToken(loginRequest.Email)

	if tokenErr != nil {
		ApiResponseWithError(writer, http.StatusInternalServerError, tokenErr.Error())
		return
	}

	message := Message{
		Code:    http.StatusOK,
		Message: "Authentication successful",
		Status:  "success",
		Data: TokenResponse{
			Email: loginRequest.Email,
			Token: token,
		},
	}

	ApiResponse(writer, http.StatusOK, message)

}
