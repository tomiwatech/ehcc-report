package controller

import (
	"gitlab.com/tomiwatech/ehcc-report/pkg/utils"
	"net/http"
)

type Message struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Status  string      `json:"status"`
	Data    interface{} `json:"data"`
}

type ErrorMessage struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Status  string      `json:"status"`
	Errors  interface{} `json:"errors"`
}

var HealthCheck = func(w http.ResponseWriter, r *http.Request) {

	responseMessage := Message{
		Code:    200,
		Message: "Welcome to EHCC Report API",
		Status:  "success",
		Data:    nil,
	}

	utils.ApiResponse(w, http.StatusOK, responseMessage)
}

var ApiVersionIndex = func(w http.ResponseWriter, r *http.Request) {

	responseMessage := Message{
		Code:    200,
		Message: "Welcome to API Version 1",
		Status:  "success",
		Data:    nil,
	}

	utils.ApiResponse(w, http.StatusOK, responseMessage)
}
