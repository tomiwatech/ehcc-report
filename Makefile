
build_and_serve: 
	docker-compose up --build

build_and_serve_detached:
	docker-compose up -d --build

spin_up_db: 
	docker-compose up -d db 

shut_down_db:
	docker-compose down 

run_integration_test:
	go test --tags=integration -v ./...

run_e2e_test:
	go test --tags=e2e -v ./...

integration_tests: spin_up_db run_integration_test

acceptance_tests: build_and_serve run_e2e_test